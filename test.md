# PHB5 Test Page
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec enim lorem. Vestibulum semper ullamcorper ornare. Donec sed mauris nec mauris pellentesque bibendum nec eget elit. Integer lacinia lorem eget metus luctus pretium. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi eu molestie libero. Curabitur vitae rutrum augue. Nulla ipsum massa, ultricies aliquet scelerisque vel, egestas ut nisi. Cras hendrerit rhoncus urna quis tempor. Phasellus vel ligula ultricies, vestibulum quam et, cursus nisi. Nam faucibus ligula eu pellentesque dapibus. Ut efficitur arcu sit amet interdum auctor. Nullam eu eros vitae sapien gravida sagittis.

## Note Blocks
> ### Take Note of This
> Nulla at finibus lorem, ac ullamcorper nibh. Pellentesque ultricies finibus dolor, quis sagittis quam tempus et. Quisque hendrerit ex ac lacus venenatis, sit amet scelerisque enim blandit. In vulputate elementum nisi et pretium. Vestibulum in tortor ut massa mollis tempor. Curabitur sed sem tincidunt, egestas lacus non, rutrum nisl.

Praesent euismod, nisi luctus pulvinar dignissim, erat risus ullamcorper sem, id laoreet augue dolor in odio.

Sed a nulla tristique nibh sollicitudin imperdiet. Cras et semper velit.

## Descriptive blocks
```
Nullam sed tincidunt justo. Nunc quis dignissim nisl, pharetra efficitur tellus. Praesent nec posuere augue, et mollis sapien. 

Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Duis eu risus id risus faucibus vestibulum. Sed tortor arcu, mollis vitae sagittis id, dapibus vel ex.
```



## Monster Stat Block

___
> ## Wailing Quail of the Sun
>*Large guy, weird neutral*
> ___
> - **Armor Class** 14
> - **Hit Points** 54(1d4 + 5)
> - **Speed** 3ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|5 (-2)|11 (+1)|18 (+4)|1 (-4)|7 (-1)|9 (+0)|
>___
> - **Condition Immunities** swagged, groovy, melancholy
> - **Senses** passive Perception 6
> - **Languages** Common
> - **Challenge** 9 (807 XP)
> ___
> ***Big Jerk.*** Thinks he is just *waaaay* better than you.
>
> ***Enormous Nose.*** This creature gains advantage on any check involving putting things in its nose.
>
> ***Onion Stench.*** Any creatures within 5 feet of this thing develops an irrational craving for onion rings.
> ### Actions
> ***Heel Jawbreaker.*** *Melee Weapon Attack:* +4 to hit, reach 5ft., one target. *Hit* 5 (1d6 + 2)
>
> ***Team Foot.*** *Melee Weapon Attack:* +4 to hit, reach 5ft., one target. *Hit* 5 (1d6 + 2)









---

Page Break

---


---
## Full-Width Monster Stat Block
---
---
> ## The Phantom Horizon
> *Gargantuan Planar Frigate (120ft x 25ft)*
>
> ---
> **Creature Capacity** 30 Crew, 10 Passengers
> **Cargo Capacity** 200 tons
> **Travel Pace** 13 / 8 / 4.5  miles per hour (air leyfoil, water leyfoil, (sail), 5 lacuna per hour (esoteric)
> **Mana Reserves** 8 gallons (96,000 drams)
> **Resupply** 768,000g (Mana), 90g (crew rations, 1 month), 1,000g (supplies, 1 month)
>
> ---
> | STR | DEX | CON | INT | WIS | CHA |
> |:-:|:-:|:-:|:-:|:-:|:-:|
> | 20 (+5) | 4 (-4) | 20 (+4) | 0 | 0 | 0 |
>
> ---
> **Damage Immunities** poison, psychic
> **Condition Immunities** blinded, charmed, deafened, exhaustion, frightened, incapacitated, paralyzed, petrified, poisoned, prone, stunned, unconscious
> 
> ### Hull
> **Armor Class** 11
> **Hit Points** Hull 120 each Port, Starboard, Fore, Aft
> **Damage Threshold** 12
> The Phantom Horizon suffers a hull breach if any one of its four hull quadrants reaches 0 hit points. If any two quadrants reach 0 hit points, it is destroyed. Where the hull is breached, personel and objects within the ship are vulnerable to direct attack.
>
> ### Control: Helm
> **Armor Class** 20
> **Hit Points**: 90
> Move up to its speed for one physical movement component, one 45 degree turn.
>
> ### Control: Esoteric
> The ship's helm is damaged, and esoteric motion must be controlled through a ritual conducted by the ship's Arcanist and at least 4 other magically adept participants. Each participant must maintian concentration during the ritual; loss of concentration causes the ship to make no progress for a turn.
>
> ### Movement - Leyfoil
> **AC** 19
> **Hit Points** 60 (x6)
> **Locomotion (Air, Water)** Air 240 ft., Water: 180ft.
> **Operation Time** 71 days (leyfoil / 6 drams per hour)
> The ship loses 40 ft. air speed and 30 ft. water speed per disabled Leyfoil. Each Leyfoil consumes one dram of mana per hour from the ship's mana reserves.
>
> ### Movement - Sail
> **AC** 11
> **Hit Points** 40 (x2)
> **Locomotion (Water)** 35 ft., 15ft. while sailing into the wind, 50ft. while sailing with the wind
> Sails are only vulnerable to ranged attack while deployed. The ship loses 10 ft. speed per 20 damage taken to its sails, to a minimum of 0.
>
> ### Weapons: Gatling Spellbow (2)
>
> *Ranged Weapon Attack* +6 to hit, *Hit* 6d6 force damage. Each use of the gatling spellbow consumes 6 drams of mana from the ship's reserves. One each port, starboard.
>
> ### Weapons: Ballista (6)
>
> *Ranged Weapon Attack* +6 to hit, *Hit* 4d6 piercing damage, *loading*. Requires 2 crew to operate. 3 each port, starboard.
>
> ### Actions
> The ship may attack with each of its manned weapons, move up to its speed with one movement component, and turn.
>






---
Page Break

---
