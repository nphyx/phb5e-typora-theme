const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const postcss = require("gulp-postcss");
const assets = require("postcss-assets");

gulp.task("css", () => {
  return gulp.src("phb5.scss")
    .pipe(sass())
    .pipe(postcss([assets({ loadPaths: ["src/theme/", "src/theme/assets/", "src/theme/fonts/"] })]))
    .pipe(gulp.dest("dist/"))
});
