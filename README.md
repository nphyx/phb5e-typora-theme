Typora PHB5 Theme
=================
A theme for styling your Typora documents like the Dungeons &amp; Dragons 5th Edition Player's Handbook, heavily inspired by and derived from [Homebrewery](https://homebrewery.naturalcrit.com).

Build Requirements
------------------
- Typora (obvs)
- NPM (needed for building stylesheets)
- GNU make (should be available in your development-tools package)

Installation
------------
Clone repository, then:

```
npm install
make
```

If you're on a Linux machine and your Typora configuration files live at `$HOME/.config/Typora`, you can also run:

```
make install
```

Otherwise, copy the contents of the `dist` folder into the `themes` folder for Typora, such that it looks like:

```
Typora/themes
  -- phb5 (folder)
  -- phb5.css
  -- (other theme stuff)
```

Usage
-----
Like Homebrewery, the PHB5 theme makes use of various clever hacks to produce notes, stat blocks, page footers, and the like. Where possible, these are identical to those found in Homeberwery, so you can copy your content from one to the other with little or no adjustment. A few things do work differently, see examples below.

### Note Blocks
Note blocks / asides use standard blockquotes, like so:

```
> # Take Note
> This is a note block
```

### Monster Stat Blocks
Monster stat blocks are denoted by a horizontal rule _immediately followed by_ a blockquote. They have a bunch of custom styling to produce something very similar to the ones found in the PHB. Example:

```
___
> ## Wailing Quail of the Sun
>*Large guy, weird neutral*
> ___
> - **Armor Class** 14
> - **Hit Points** 54(1d4 + 5)
> - **Speed** 3ft.
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|5 (-2)|11 (+1)|18 (+4)|1 (-4)|7 (-1)|9 (+0)|
>___
> - **Condition Immunities** swagged, groovy, melancholy
> - **Senses** passive Perception 6
> - **Languages** Common
> - **Challenge** 9 (807 XP)
> ___
> ***Big Jerk.*** Thinks he is just *waaaay* better than you.
>
> ***Enormous Nose.*** This creature gains advantage on any check involving putting things in its nose.
>
> ***Onion Stench.*** Any creatures within 5 feet of this thing develops an irrational craving for onion rings.
> ### Actions
> ***Heel Jawbreaker.*** *Melee Weapon Attack:* +4 to hit, reach 5ft., one target. *Hit* 5 (1d6 + 2)
>
> ***Team Foot.*** *Melee Weapon Attack:* +4 to hit, reach 5ft., one target. *Hit* 5 (1d6 + 2)
```

### Full-Width Monster Stat Block
As Monster Stat Block, but with two HRs preceding:

```
---
---
> ## Monster Name
> (...etc)
```

### Descriptive Blocks
Blocks meant to be read aloud to players, decorated differently from notes. Because Typora cannot support HTML wrapper elements or custom Homebrewery extensions, these are implemented as three horizontal rules followed by a blockquote:

```
---
---
---
> This place is wild.
> Oh no, now something's happening!
```

### Column Breaks
Typora interferes with Homebrewery's approach to column breaks, so they're implemented differently here, as an empty line followed by a subscript:

```

~break~
```

The text in between the tildes (`~`) can be whatever you want. It will be hidden on the page.


### Page Breaks
Typora doesn't support the special `\page` instruction used by Homebrewery. Instead, use a horizontal rule, followed by text, followed by a blank space, followed by another horizontal rule, like so (it must be _exactly_ like this):

```
---
Some Footer Text

---
```

PHB5 will create a stylized page break using the text as the footer, as well as a decorative header for the following page. It generates page numbers and inserts them automatically.

#### Notes/Complications
Typora, and Markdown in general, has no concept of page length. You will need to insert your page breaks manually in appropriate places, and add blank lines to fill in partially empty pages.

Todo
----
This is a very new project, so some features are not yet ported from Homebrewery:

- class blocks
- spell tables
- probably some other things

### Known Issues
- exporting to PDF needs some work

Thanks
------
Thanks to Homebrewery for doing all the hard work. This is just a port with a few tweaks and additional features.

License
-------
MIT
