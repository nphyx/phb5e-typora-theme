SHELL:=/bin/bash

all: clean styles
clean:
	rm -rf dist
	mkdir -p dist
styles:
	echo "building stylesheets..."
	gulp css
test: clean styles
	rm -rf test/*
	cp -R src/html test/html
	cp -R src/theme test/theme
	cp dist/phb5.css test/theme/test.css
	brave test/html/*.html
install:
	rm -rf ~/.config/Typora/themes/phb5
	rm -rf ~/.config/Typora/themes/phb5.css
	cp -R dist/* ~/.config/Typora/themes/
